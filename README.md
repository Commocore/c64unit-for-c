
c64unit for C
=============

The ultimate unit test framework for Commodore 64, C language version.


![c64unit](http://www.commocore.com/images/external/c64unit/c64unit-logo.png)


## Contents

1. [About](#markdown-header-1-about).

2. [Installation](#markdown-header-2-installation).

3. [Organizing tests](#markdown-header-3-organizing-tests).

4. [Creating your first test suite](#markdown-header-4-creating-your-first-test-suite).

5. [Functions](#markdown-header-5-functions).

6. [Compiling](#markdown-header-6-compiling).

7. [License](#markdown-header-7-license).


# 1. About.

*c64unit for C* test framework allows you to test your code within [cc65](https://github.com/cc65/cc65), the
cross-development package for 65(c)02 systems, including macro assembler, a C compiler, linker and several other tools.

With few handy assertions in a toolbox, testing your code is just simple!

Test suite examples written in C are available here:
[https://bitbucket.org/Commocore/c64unit-for-c-examples](https://bitbucket.org/Commocore/c64unit-for-c-examples).

*c64unit* test framework supports also testing for programs written in pure assembly code. Please visit
[https://bitbucket.org/Commocore/c64unit](https://bitbucket.org/Commocore/c64unit) to get _c64unit_ version
which supports cross-assemblers like *64tass*, *Kick Assembler*, *DASM*, *ACME* or *ca65*.

Note: *c64unit for C* is in very early alpha stage.


# 2. Installation.

To add *c64unit for C* into your project, just copy `install/c64unit-for-c-dependency.bat` (Windows), or
`install/c64unit-for-c-dependency.sh` (Linux, Mac) script file from this repository to your main project folder.
Execution of the script file creates `vendor/c64unit-for-c` folder and clones this repository using *git* version
control system. It's recommended to list `vendor` folder in `.gitignore` file.


# 3. Organizing tests.

You can organize your tests in a test suite focused on a particular cycle of the project or its scope, etc.
You can share tests across different test suites, or you can have just one test suite - it's up to you.

    .
    +-- src
        +-- (your code here)
    +-- tests
        +-- build (this folder can be added to .gitignore)
            +-- test-suite.prg (compiled test suite output file)
        +-- test-cases
            +-- functionality-1
                +-- test-green-feature.c
                +-- test-orange-feature.c
                +-- ...
            +-- functionality-2
                +-- test-algorithm-feature.c
                +-- ...
        +-- test-suite.c
    +-- vendor
        +-- c64unit-for-c
            +-- (content of this package)
    +-- c64unit-for-c-dependency.sh


# 4. Creating your first test suite.

Creating a test suite file is simple, let's name it `tests/test-suite.c`:

    #include <c64unit.h>
    #include "./../src/green-function.c"
    #include "./test-cases/functionality-1/test-green-feature.c"
    
    int main(void) {
        c64unit(true);
        
        // Examine test cases
        testGreenFeature();
        
        return c64unitExit();
    }


Test suite refers to test file (e.g. `tests/test-cases/functionality-1/test-green-feature.c`) which can look this way:

    void testGreenFeature() {
        c64unit_assertIntEqual(2018, greenFunction(18));
    }
    

# 5. Functions.

All functions and they signatures are available in `src/c64unit.h` file.


# 6. Compiling.

    cd <your-project>\tests
    cl65 -g -Oi -t c64 --include-dir ..\vendor\c64unit-for-c\src --obj ..\vendor\build\c64unit.o test-suite.c -o build\test-suite.prg


# 7. License.

Copyright © 2018, Bartosz Żołyński, [Commocore](http://www.commocore.com).

Released under the [License](LICENSE).

All rights reserved.
