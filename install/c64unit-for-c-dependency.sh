#!/usr/bin/env bash
if [ -d vendor/c64unit-for-c ];
then
    cd vendor/c64unit-for-c
    git pull http://Commocore@bitbucket.org/Commocore/c64unit-for-c.git master
    cd ../../
else
    git clone --origin c64unit-for-c http://Commocore@bitbucket.org/Commocore/c64unit-for-c.git vendor/c64unit-for-c
fi
