@echo off
IF EXIST vendor\c64unit-for-c (
    cd vendor\c64unit-for-c
    git pull http://Commocore@bitbucket.org/Commocore/c64unit-for-c.git master
    cd ..\..\
) ELSE (
    git clone --origin c64unit-for-c http://Commocore@bitbucket.org/Commocore/c64unit-for-c.git vendor/c64unit-for-c
)
@echo on
