
void displayCustomMessage(char * message) {
	gotoxy(0, 22);
	printf(message);
}

void displayAssertMessage(const char * customMessage) {
	gotoxy(0, 23);
	revers(1);
	printf(customMessage);
	revers(0);
}

void displayAssertIntDetailsMessage(int expected, int actual) {
	gotoxy(0, 24);
	if (c64unit_dataSetLength) {
		printf("data #%d, ", c64unit_dataSet);
	}
	printf("expected: %d, actual: %d.", expected, actual);
}

void displayAssertCharDetailsMessage(char * expected, char * actual) {
	gotoxy(0, 24);
	if (c64unit_dataSetLength) {
		printf("data #%d, ", c64unit_dataSet);
	}
	printf("expected: %s, actual: %s.", expected, actual);
}
